# PureArchive

This is a stripped down version of [Hyperkitty](https://gitlab.com/mailman/hyperkitty/).

Please note that this is a work-in-progress. You shouldn't use it in production yet!

The goal is to have a simple archiver.
To achieve this we try to
* Only add features that are essential to an archive
* Minimize dependencies
* Keep setup as simple as possible
