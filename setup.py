#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys

from setuptools import setup, find_packages


# extract the version number without importing the package
with open('hyperkitty/__init__.py') as fp:
    for line in fp:
        mo = re.match("""VERSION\s*=\s*['"](?P<version>[^'"]+?)['"]""", line)
        if mo:
            __version__ = mo.group('version')
            break
    else:
        print('No version number found')
        sys.exit(1)


# Requirements
REQUIRES = [
    "django_mailman3>=1.2.0a2",
    "pytz>=2012",
    "mailmanclient>=3.1.1",
    "python-dateutil >= 2.0",
    "django-widgets-improved",
    "Django>=1.11",
]


setup(
    name="PureArchive",
    version=__version__,
    description="A web interface to access GNU Mailman v3 archives",
    long_description="A Light Mailman Archiver",
    author='Simon Hanna',
    author_email='simhnna@gmail.com',
    url="https://gitlab.com/thelinuxguy/purearchive",
    license="GPLv3",
    classifiers=[
        "Framework :: Django",
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Topic :: Communications :: Email :: Mailing List Servers",
        "Programming Language :: Python :: 3",
        "Programming Language :: JavaScript",
        ],
    keywords='email',
    # packages=find_packages(exclude=["*.test", "test", "*.test.*"]),
    packages=find_packages(),
    include_package_data=True,
    install_requires=REQUIRES,
    )
