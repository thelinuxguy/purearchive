from django.contrib import admin


from hyperkitty.models import mailinglist, email, thread, sender


admin.site.register(mailinglist.MailingList)
admin.site.register(email.Email)
admin.site.register(thread.Thread)
admin.site.register(sender.Sender)
