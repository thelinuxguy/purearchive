# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-18 20:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hyperkitty', '0017_auto_20180317_2355'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='user',
        ),
        migrations.RemoveField(
            model_name='email',
            name='timezone',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='date_active',
        ),
        migrations.AddField(
            model_name='email',
            name='raw_message',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Profile',
        ),
    ]
