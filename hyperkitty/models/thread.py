# -*- coding: utf-8 -*-
#
# Copyright (C) 2014-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#

from django.db import models
from django.utils.timezone import now


import logging
logger = logging.getLogger(__name__)


class Thread(models.Model):
    """
    A thread of archived email, from a mailing-list. It is identified by both
    the list name and the thread id.
    """
    mailinglist = models.ForeignKey(
        # Delete the model if the MailingList is deleted.
        "MailingList", related_name="threads", on_delete=models.CASCADE)
    thread_id = models.CharField(max_length=255, db_index=True)
    date_active = models.DateTimeField(db_index=True, default=now)
    starting_email = models.OneToOneField(
        "Email", related_name="started_thread", null=True,
        on_delete=models.SET_NULL)

    def __str__(self):
        if self.starting_email:
            return self.starting_email.subject
        return "NO starting email"

    class Meta:
        unique_together = ("mailinglist", "thread_id")

    def replies_after(self, date):
        return self.emails.filter(date__gt=date)

    @property
    def emails_count(self):
        return 10000

    @property
    def subject(self):
        return self.starting_email.subject

    @property
    def prev_thread(self):  # TODO: Make it a relationship
        return Thread.objects.filter(
                mailinglist=self.mailinglist,
                date_active__lt=self.date_active
            ).order_by("-date_active").first()

    @property
    def next_thread(self):  # TODO: Make it a relationship
        return Thread.objects.filter(
                mailinglist=self.mailinglist,
                date_active__gt=self.date_active
            ).order_by("date_active").first()

    def find_starting_email(self):
        # Find and set the staring email if it was not specified
        from .email import Email  # circular import
        if self.starting_email is not None:
            return
        try:
            self.starting_email = self.emails.get(parent_id__isnull=True)
        except Email.DoesNotExist:
            self.starting_email = self.emails.order_by("date").first()
