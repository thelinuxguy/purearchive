# -*- coding: utf-8 -*-
#
# Copyright (C) 2014-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#

import re
from email.message import EmailMessage

from django.urls import reverse
from django.db import models, IntegrityError
from django.utils.timezone import now, get_fixed_timezone

from .mailinglist import MailingList
from .thread import Thread

import logging
logger = logging.getLogger(__name__)


class Email(models.Model):
    """
    An archived email, from a mailing-list. It is identified by both the list
    name and the message id.
    """
    mailinglist = models.ForeignKey(
        "MailingList", related_name="emails", on_delete=models.CASCADE)
    message_id = models.CharField(max_length=255, db_index=True)
    message_id_hash = models.CharField(max_length=255, db_index=True)
    sender = models.ForeignKey(
        "Sender", related_name="emails", on_delete=models.CASCADE)
    sender_name = models.CharField(max_length=255, null=True, blank=True)
    subject = models.CharField(max_length=512, db_index=True)
    content = models.TextField()
    date = models.DateTimeField(db_index=True)
    in_reply_to = models.CharField(
        max_length=255, null=True, blank=True, db_index=True)
    # Delete behavior is handled by on_pre_delete()
    parent = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.DO_NOTHING,
        related_name="children")
    thread = models.ForeignKey(
        "Thread", related_name="emails", on_delete=models.CASCADE, null=True)
    archived_date = models.DateTimeField(default=now, db_index=True)
    thread_depth = models.IntegerField(default=0)
    thread_order = models.IntegerField(null=True, blank=True, db_index=True)

    raw_message = models.TextField()

    ADDRESS_REPLACE_RE = re.compile(r"([\w.+-]+)@([\w.+-]+)")

    def __lt__(self, other):
        return self.date < other.date

    class Meta:
        unique_together = ("mailinglist", "message_id")

    def get_absolute_url(self):
        return reverse('hk_message_index', args=[self.mailinglist.name, self.message_id_hash])

    def __str__(self):
        return self.subject

    def set_parent(self, parent):
        if self.id == parent.id:
            raise ValueError("An email can't be its own parent")
        # Compute the subthread
        subthread = [self]

        def _collect_children(current_email):
            children = list(current_email.children.all())
            if not children:
                return
            subthread.extend(children)
            for child in children:
                _collect_children(child)
        _collect_children(self)
        # now set my new parent value
        old_parent_id = self.parent_id
        self.parent = parent
        self.save(update_fields=["parent_id"])
        # If my future parent is in my current subthread, I need to set its
        # parent to my current parent
        if parent in subthread:
            parent.parent_id = old_parent_id
            parent.save(update_fields=["parent_id"])
            # do it after setting the new parent_id to avoid having two
            # parent_ids set to None at the same time (IntegrityError)
        if self.thread_id != parent.thread_id:
            # we changed the thread, reattach the subthread
            former_thread = self.thread
            for child in subthread:
                child.thread = parent.thread
                child.save(update_fields=["thread_id"])
                if child.date > parent.thread.date_active:
                    parent.thread.date_active = child.date
            parent.thread.save()
            # if we were the starting email, or former thread may be empty
            if former_thread.emails.count() == 0:
                former_thread.delete()

    def as_message(self, escape_addresses=True):
        # http://wordeology.com/computer/how-to-send-good-unicode-email-with-python.html
        # http://stackoverflow.com/questions/31714221/how-to-send-an-email-with-quoted
        # http://stackoverflow.com/questions/9403265/how-do-i-use-python/9509718#9509718
        msg = EmailMessage()

        # Headers
        unixfrom = "From %s %s" % (
            self.sender.address, self.archived_date.strftime("%c"))
        assert isinstance(self.sender.address, str)
        header_from = self.sender.address
        if self.sender_name and self.sender_name != self.sender.address:
            header_from = "%s <%s>" % (self.sender_name, header_from)
        header_to = self.mailinglist.name
        msg.set_unixfrom(unixfrom)
        headers = (
            ("From", header_from),
            ("To", header_to),
            ("Subject", self.subject),
            )
        for header_name, header_value in headers:
            msg[header_name] = header_value
        tz = get_fixed_timezone(self.timezone)
        header_date = self.date.astimezone(tz).replace(microsecond=0)
        # Date format: http://tools.ietf.org/html/rfc5322#section-3.3
        msg["Date"] = header_date.strftime("%a, %d %b %Y %H:%M:%S %z")
        msg["Message-ID"] = "<%s>" % self.message_id
        if self.in_reply_to:
            msg["In-Reply-To"] = self.in_reply_to

        # Body
        content = self.ADDRESS_REPLACE_RE.sub(r"\1(a)\2", self.content)

        # Enforce `multipart/mixed` even when there are no attachments
        # Q: Why are all emails supposed to be multipart?
        if self.attachments.count() == 0:
            msg.set_content(content, subtype='plain')
            msg.make_mixed()

        # Attachments
        for attachment in self.attachments.order_by("counter"):
            mimetype = attachment.content_type.split('/', 1)
            msg.add_attachment(attachment.content, maintype=mimetype[0],
                               subtype=mimetype[1], filename=attachment.name)

        return msg

    @property
    def display_fixed(self):
        return "@@" in self.content

    def on_post_created(self):
        self.thread.on_email_added(self)
        self.mailinglist.on_email_added(self)

    def on_pre_save(self):
        # Link to the thread
        if self.thread_id is None:
            # Create the thread if not found
            thread, _thread_created = Thread.objects.get_or_create(
                mailinglist=self.mailinglist,
                thread_id=self.message_id_hash)
            self.thread = thread
        # Make sure there is only one email with parent_id == None in a thread
        if self.parent_id is not None:
            return
        starters = Email.objects.filter(
                thread=self.thread, parent_id__isnull=True
            ).values_list("id", flat=True)
        if len(starters) > 0 and list(starters) != [self.id]:
            raise IntegrityError("There can be only one email with "
                                 "parent_id==None in the same thread")

    def on_pre_delete(self):
        # Reset parent_id
        children = self.children.order_by("date")
        if not children:
            return
        if self.parent is None:
            #  Temporarily set the email's parent_id to not None, to allow the
            #  next email to be the starting email (there's a check on_save for
            #  duplicate thread starters)
            self.parent = self
            self.save(update_fields=["parent"])
            starter = children[0]
            starter.parent = None
            starter.save(update_fields=["parent"])
            children.all().update(parent=starter)
        else:
            children.update(parent=self.parent)

    def on_post_delete(self):
        try:
            thread = Thread.objects.get(id=self.thread_id)
        except Thread.DoesNotExist:
            pass
        else:
            thread.on_email_deleted(self)
        try:
            mlist = MailingList.objects.get(pk=self.mailinglist_id)
        except MailingList.DoesNotExist:
            pass
        else:
            mlist.on_email_deleted(self)


class Attachment(models.Model):
    email = models.ForeignKey(
        "Email", related_name="attachments", on_delete=models.CASCADE)
    counter = models.SmallIntegerField()
    name = models.CharField(max_length=255)
    content_type = models.CharField(max_length=255)
    encoding = models.CharField(max_length=255, null=True)
    size = models.IntegerField()
    content = models.BinaryField()

    class Meta:
        unique_together = ("email", "counter")
