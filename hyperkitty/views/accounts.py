# -*- coding: utf-8 -*-
#
# Copyright (C) 2014-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#


from allauth.account.models import EmailAddress
try:
    from django.core.urlresolvers import reverse
except ImportError:
    # For Django 2.0+
    from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django_mailman3.lib.mailman import (
    get_mailman_client, get_mailman_user_id, get_subscriptions)
from django_mailman3.lib.paginator import paginate

from hyperkitty.models import MailingList, Email


import logging
logger = logging.getLogger(__name__)


@login_required
def user_profile(request):
    # Get the messages and paginate them
    email_addresses = EmailAddress.objects.filter(
        user=request.user).values("email")
    last_posts = Email.objects.filter(
        sender__address__in=email_addresses).order_by("-date")
    last_posts = paginate(last_posts,
                          request.GET.get("lppage"),
                          request.GET.get("lpcount", "10"))

    context = {
        "last_posts": last_posts,
        "subpage": "profile",
    }
    return render(request, "hyperkitty/user_profile/profile.html", context)


@login_required
def subscriptions(request):
    mm_user_id = get_mailman_user_id(request.user)
    subs = []
    for mlist_id in get_subscriptions(request.user):
        try:
            mlist = MailingList.objects.get(list_id=mlist_id)
        except MailingList.DoesNotExist:
            mlist = None  # no archived email yet
        all_posts_url = None
        if mlist is not None:
            list_name = mlist.name
            if mm_user_id is not None:
                all_posts_url = "%s?list=%s" % (
                    reverse("hk_user_posts", args=[mm_user_id]),
                    mlist.name)
        else:
            list_name = get_mailman_client().get_list(mlist_id).fqdn_listname
        subs.append({
            "list_name": list_name,
            "mlist": mlist,
            "all_posts_url": all_posts_url,
        })
    return render(request, 'hyperkitty/user_profile/subscriptions.html', {
                "subscriptions": subs,
                "subpage": "subscriptions",
            })
