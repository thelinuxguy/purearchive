# -*- coding: utf-8 -*-
#
# Copyright (C) 2014-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#

from django.db.models import Q
from django.shortcuts import render, redirect
from django_mailman3.lib.paginator import paginate

from hyperkitty.lib.view_helpers import is_mlist_authorized
from hyperkitty.models import MailingList


def index(request):
    mlists = MailingList.objects.all()
    if not request.user.is_superuser:
        mlists = mlists.filter(advertised=True)
    sort_mode = request.GET.get("sort", "popular")

    # Name filtering
    name_filter = request.GET.get('name')
    if name_filter:
        sort_mode = "name"
        mlists = mlists.filter(
            Q(name__icontains=name_filter) |
            Q(display_name__icontains=name_filter)
            )
        if mlists.count() == 1:
            return redirect(
                'hk_list_overview', mlist_fqdn=mlists.first().name)

    # Sorting
    mlists = mlists.order_by("name")

    mlists = paginate(mlists, request.GET.get('page'),
                      request.GET.get('count'))

    # Permissions
    for mlist in mlists:
        if not mlist.is_private:
            mlist.can_view = True
        else:
            if is_mlist_authorized(request, mlist):
                mlist.can_view = True
            else:
                mlist.can_view = False

    context = {
        'view_name': 'all_lists',
        'all_lists': mlists,
        'sort_mode': sort_mode,
        }
    return render(request, "hyperkitty/index.html", context)
