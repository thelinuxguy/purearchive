# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aamir Khan <syst3m.w0rm@gmail.com>
# Author: Aurelien Bompard <abompard@fedoraproject.org>
#

from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from hyperkitty.views import (
    index, accounts, mlist, message, thread, mailman, compat)


# flake8: noqa


# List archives and overview
list_patterns = [
    url(r'^(?P<year>\d{4})/(?P<month>\d\d?)/(?P<day>\d\d?)/$',
        mlist.archives, name='hk_archives_with_day'),
    url(r'^(?P<year>\d{4})/(?P<month>\d\d?)/$',
        mlist.archives, name='hk_archives_with_month'),
    url(r'^latest$', mlist.archives, name='hk_archives_latest'),
    url(r'^$', mlist.overview, name='hk_list_overview'),
    url(r'^export/(?P<filename>[^/]+)\.mbox.gz$',
        mlist.export_mbox, name='hk_list_export_mbox'),
]


# Messages
message_patterns = [
    url(r'^$', message.index, name='hk_message_index'),
    url(r'^attachment/(?P<counter>\d+)/(?P<filename>.+)$',
        message.attachment, name='hk_message_attachment'),
    url(r'^reply$', message.reply, name='hk_message_reply'),
    url(r'^delete$', message.delete, name='hk_message_delete'),
]


# Threads
thread_patterns = [
    url(r'^$', thread.thread_index, name='hk_thread'),
    url(r'^reattach$', thread.reattach, name='hk_thread_reattach'),
    url(r'^delete$', message.delete, name='hk_thread_delete'),
]

urlpatterns = [
    # Index
    url(r'^$', index.index, name='hk_root'),

    # User profile
    url(r'^profile/', include([
        url(r'^$', accounts.user_profile, name='hk_user_profile'),
        url(r'^subscriptions$', accounts.subscriptions,
            name='hk_user_subscriptions'),
    ])),

    # List archives and overview
    url(r'^list/(?P<mlist_fqdn>[^/@]+@[^/@]+)/', include(list_patterns)),

    # Messages
    url(r'^list/(?P<mlist_fqdn>[^/@]+@[^/@]+)/message/'
        r'(?P<message_id_hash>\w+)/', include(message_patterns)),
    url(r'^list/(?P<mlist_fqdn>[^/@]+@[^/@]+)/message/new$',
        message.new_message, name='hk_message_new'),

    # Threads
    url(r'^list/(?P<mlist_fqdn>[^/@]+@[^/@]+)/thread/(?P<threadid>\w+)/',
        include(thread_patterns)),

    # Mailman archiver API
    url(r'^api/mailman/urls$', mailman.urls, name='hk_mailman_urls'),
    url(r'^api/mailman/archive$', mailman.archive, name='hk_mailman_archive'),

    # Mailman 2.X compatibility
    url(r'^listinfo/?$', compat.summary),
    url(r'^listinfo/(?P<list_name>[^/]+)/?$', compat.summary),
    url(r'^pipermail/(?P<list_name>[^/]+)/?$', compat.summary),
    url(r'^pipermail/(?P<list_name>[^/]+)/(?P<year>\d\d\d\d)-(?P<month_name>\w+)/?$', compat.arch_month),
    url(r'^pipermail/(?P<list_name>[^/]+)/(?P<year>\d\d\d\d)-(?P<month_name>\w+)/(?P<summary_type>[a-z]+)\.html$', compat.arch_month),
    url(r'^pipermail/(?P<list_name>[^/]+)/(?P<year>\d\d\d\d)-(?P<month_name>\w+)\.txt.gz', compat.arch_month_mbox),
    #url(r'^pipermail/(?P<list_name>[^/]+)/(?P<year>\d\d\d\d)-(?P<month_name>\w+)/(?P<msg_num>\d+)\.html$', compat.message),
    url(r'^list/(?P<list_name>[^@]+)@[^/]+/(?P<year>\d\d\d\d)-(?P<month_name>\w+)/?$', compat.arch_month),
    #url(r'^list/(?P<list_name>[^@]+)@[^/]+/(?P<year>\d\d\d\d)-(?P<month_name>\w+)/(?P<msg_num>\d+)\.html$', compat.message),

    # URL compatibility with previous versions
    url(r'^list/(?P<list_id>[^@/]+)/', compat.redirect_list_id),
    url(r'^lists/', compat.redirect_lists),

]