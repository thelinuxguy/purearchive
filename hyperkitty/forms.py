# -*- coding: utf-8 -*-
# Copyright (C) 2012-2017 by the Free Software Foundation, Inc.
#
# This file is part of HyperKitty.
#
# HyperKitty is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# HyperKitty is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# HyperKitty.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Aamir Khan <syst3m.w0rm@gmail.com>
# Author: Aurélien Bompard <abompard@fedoraproject.org>
#

from django import forms
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class AttachmentFileInput(forms.FileInput):
    attach_first_text = _('Attach a file')
    attach_another_text = _('Attach another file')
    rm_text = _('Remove this file')
    template = """
<span class="attach-files-template">
    %(input)s <a href="#" title="%(rm_text)s">(-)</a>
</span>
<span class="attach-files"></span>
<a href="#" class="attach-files-first">%(attach_first_text)s</a>
<a href="#" class="attach-files-add">%(attach_another_text)s</a>
"""

    def render(self, name, value, attrs=None):
        substitutions = {
            'attach_first_text': self.attach_first_text,
            'attach_another_text': self.attach_another_text,
            'rm_text': self.rm_text,
        }
        substitutions['input'] = super(AttachmentFileInput, self).render(
            name, value, attrs)
        return mark_safe(self.template % substitutions)


class ReplyForm(forms.Form):
    newthread = forms.BooleanField(label="", required=False)
    subject = forms.CharField(
        label="", required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'New subject', 'class': 'form-control'}))
    message = forms.CharField(
        label="",
        widget=forms.Textarea(attrs={'class': 'form-control'}))
    sender = forms.ChoiceField(
        label="", required=False,
        widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    #  attachment = forms.FileField(required=False, widget=AttachmentFileInput)


class PostForm(forms.Form):

    subject = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.ChoiceField(
        label="", required=False,
        widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    # attachment = forms.FileField(required=False, label="",
    #                              widget=AttachmentFileInput)


class MessageDeleteForm(forms.Form):
    email = forms.ModelMultipleChoiceField(
        queryset=None, widget=forms.ModelMultipleChoiceField.hidden_widget,
        )
